// Import Modules
import { UESRPG } from "./config.js";
import { SimpleActor } from "./actor.js";
import { npcSheet } from "./npc-sheet.js";
import { SimpleActorSheet } from "./actor-sheet.js";
import { merchantSheet } from "./merchant-sheet.js";
import { SimpleItem } from "./item.js";
import { SimpleItemSheet } from "./item-sheet.js";
import { SystemCombat } from "./combat.js";


/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */

Hooks.once("init", async function() {
  console.log(`Initializing UESRPG System`);

	/**
	 * Set an initiative formula for the system
	 * @type {String}
	 */
	CONFIG.Combat.initiative = {
    formula: "1d6 + @initiative.base",
    decimals: 0
  };

  // Set up custom combat functionality for the system.
  CONFIG.Combat.documentClass = SystemCombat;

  // Record Configuration Values
	CONFIG.UESRPG = UESRPG;

	// Define custom Entity classes
  CONFIG.Actor.documentClass = SimpleActor;
  CONFIG.Item.documentClass = SimpleItem;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Items.unregisterSheet("core", ItemSheet);
  Actors.registerSheet("deadend-d100-FR", SimpleActorSheet,
    {types: ["character"],
    makeDefault: true,
    label: "Default UESRPG Character Sheet"
    });
  Items.registerSheet("deadend-d100-FR", SimpleItemSheet,
    {
    makeDefault: true,
    label: "Default UESRPG Item Sheet"
    });
  Actors.registerSheet("deadend-d100-FR", npcSheet, {
    types: ["npc"],
    makeDefault: true,
    label: "Default UESRPG NPC Sheet"
    });
  Actors.registerSheet("deadend-d100-FR", merchantSheet, {
    types: ["npc"],
    makeDefault: false,
    label: "Default UESRPG Merchant Sheet"
  });

  // Register system settings
  function delayedReload() {window.setTimeout(() => location.reload(), 500)}

  game.settings.register("deadend-d100-FR", "legacyUntrainedPenalty", {
    name: "Legacy Untrained Penalty",
    hint: "Checking this option enables the UESRPG v2 penalty for Untrained skills at -20 instead of the standard -10.",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: delayedReload
  });

  game.settings.register("deadend-d100-FR", "startUpDialog", {
    name: "Do Not Show Dialog on Startup",
    hint: "Checking this box hides the startup popup dialog informing the user on additional game resources.",
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  game.settings.register("deadend-d100-FR", "automateMagicka", {
    name: "Automate Magicka Cost",
    hint: "Automatically deduct the cost of a spell after cost calculation from the token/character's current magicka.",
    scope: "world",
    config: true,
    default: false,
    type: Boolean,
    onChange: delayedReload
  });

  game.settings.register("deadend-d100-FR", "automateActionPoints", {
    name: "Automate Action Points",
    hint: `Automatically set all combatants' AP to max at the start of each encounter.
           Automatically set a combatant's AP to max at the start of their turn (except during the first round).`,
    scope: "world",
    config: true,
    default: false,
    type: Boolean
  });

  game.settings.register("deadend-d100-FR", "npcENCPenalty", {
    name: "NPC's Suffer Encumbrance Penalties",
    hint: "If checked, NPC's suffer from the same overencumbrance penalties that player characters do. Otherwise, they suffer no ENC Penalties.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: delayedReload
  });

  game.settings.register("deadend-d100-FR", "sortAlpha", {
    name: "Sort Actor Items Alphabetically",
    hint: "If checked, Actor items are automatically sorted alphabetically. Otherwise, items are not sorted and are organized manually.",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
    onChange: delayedReload
  });

  const startUpFunction = () => {
    const startUpDialog = game.settings.get("deadend-d100-FR", "startUpDialog");
    let discordIcon = `<i class="fab fa-discord fa-2x"></i>`;
    let patreonIcon = `<i class="fab fa-patreon fa-2x"></i>`;
    let gitLabIcon = `<i class="fab fa-gitlab fa-2x"></i>`;
    let patreonLink = "Patreon";
    let discordLink = "Discord Channel";
    let gitLabLink = 'GitLab Repo'
    let contentModLink = "https://github.com/95Gman/UESRPG-revised";

 /*   let popup = new Dialog({
      title: "Welcome to the UESRPG Foundry System!",
      content: `<form style="height: 100%;>
        <div class="dialogForm" style="padding: 5px">

          <div style="text-align: center; margin: 5px; padding: 5px; background-color: rgba(78, 78, 78, 0.137);">
            <span style="margin-left: 10px; margin-right: 10px;">
              ${patreonIcon.link("https://www.patreon.com/bePatron?u=30258550")}
            </span>
            <span style="margin-left: 10px; margin-right: 10px;">
              ${discordIcon.link("https://discord.gg/pBRJwy3Ec5")}
            </span>
            <span style="margin-left: 10px; margin-right: 10px;">
              ${gitLabIcon.link("https://gitlab.com/DogBoneZone/uesrpg-3e")}
            </span>
          </div>

          <div style="margin: 5px; padding: 5px; background-color: rgba(78, 78, 78, 0.137);">
              <h2 style="text-align: center;">Join the Community!</h2>
              <label>
                Hey adventurer! Thanks for taking the time to check out the UESRPG system on Foundry. UESRPG is
                an incredible game developed by a team of dedicated and talented designers. You can find out more about the game,
                download the free rulebooks, and interact with our lively community on the ${discordLink.link("https://discord.gg/pBRJwy3Ec5")}.
              </label>

              <p></p>

              <label>
                If you want to support further development of this system, please consider supporting me on ${patreonLink.link("https://www.patreon.com/bePatron?u=30258550")}.
                Thank you, and enjoy the UESRPG System!
              </<label>

              <p></p>

            <h2 style="text-align: center;">Recommended Game Content</h2>
              <label>
                The following modules/content were created by some dedicated community members and are <b>highly recommended</b>
                as they provide hundreds of pre-built items, NPC's, and much more.
              </label>
              <ul>
                <li>${contentModLink.link("https://github.com/95Gman/UESRPG-revised")}</li>
              </ul>
          </div>

          <div style="overflow-y: scroll; height: 300px; margin: 5px; padding: 5px; background-color: rgba(78, 78, 78, 0.137);">
            <h2 style="text-align: center;">v${game.system.data.version} Changelog</h2>

                <h3>General Changes</h3>
                  <ul>
                      <li>
                          Fixed issue where NPC Race field could not be edited correctly and fixed issue where hitting the Enter/Return key while on an NPC sheet registered a click and incremented the qty 
                          of the first item on that NPC's item list.
                      </li>
                      <li>
                          Fixed issue where Items that modified characteristics were not correctly affecting the value of Skills that use that characteristic as a base. 
                      </li>
                  </ul>
          </div>

          <div style="padding: 5px;">
            <i>You can disable this popup message in the System Settings and checking the box to not show this again.</i>
          </div>
        </div>
        </form>`,
      buttons: {
            one: {
              label: "Close"
            }
          },
      default: "one",
      close: html => console.log()
    });*/
    popup.position.width = 650;
    popup.position.height = 750;
    popup.render(true);
  }

  if (game.settings.get('deadend-d100-FR', 'startUpDialog') === false) {startUpFunction()}

});
