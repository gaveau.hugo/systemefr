/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */

export class SimpleActor extends Actor {
  async _preCreate(data, options, user) {
    await super._preCreate(data, options, user);
    if (data.type === "character") {
      this.data.token.update({vision: true, actorLink: true, disposition: 1})
      let skillPack = game.packs.get("deadend-d100-FR.standard-skills");
      let collection = await skillPack.getDocuments();
      collection.sort(function (a, b) {
        let nameA = a.name.toUpperCase();
        let nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        } if (nameA > nameB) {
          return 1;
        }
        return 0
      });
      this.data.update({
        items: collection.map(i => i.toObject())
      });

      this.data.update({'data.size': 'standard'})
    }
  }

  prepareData() {
    super.prepareData();

    const actorData = this.data;
    const data = actorData.data;
    const flags = actorData.flags;

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (actorData.type === 'character') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNPCData(actorData);
  }

  /**
   * Prepare Character type specific data
   */
  _prepareCharacterData(actorData) {
    const data = actorData.data;

    //Add bonuses from items to Characteristics
    data.characteristics.str.total = data.characteristics.str.base + this._strBonusCalc(actorData);
    data.characteristics.end.total = data.characteristics.end.base + this._endBonusCalc(actorData);
    data.characteristics.agi.total = data.characteristics.agi.base + this._agiBonusCalc(actorData)- this._armorWeight(actorData);
    data.characteristics.int.total = data.characteristics.int.base + this._intBonusCalc(actorData);
    data.characteristics.wp.total = data.characteristics.wp.base + this._wpBonusCalc(actorData);
    data.characteristics.prc.total = data.characteristics.prc.base + this._prcBonusCalc(actorData);
    data.characteristics.prs.total = data.characteristics.prs.base + this._prsBonusCalc(actorData);
    data.characteristics.lck.total = data.characteristics.lck.base + this._lckBonusCalc(actorData);
    data.characteristics.san.total = data.characteristics.san.base + this._sanBonusCalc(actorData);
    data.characteristics.sag.total = data.characteristics.sag.base + this._sagBonusCalc(actorData);


    //Characteristic Bonuses
    var strBonus = Math.floor(data.characteristics.str.total / 10);
    var endBonus = Math.floor(data.characteristics.end.total / 10);
    var agiBonus = Math.floor(data.characteristics.agi.total / 10);
    var intBonus = Math.floor(data.characteristics.int.total / 10);
    var wpBonus = Math.floor(data.characteristics.wp.total / 10);
    var prcBonus = Math.floor(data.characteristics.prc.total / 10);
    var prsBonus = Math.floor(data.characteristics.prs.total / 10);
    var lckBonus = Math.floor(data.characteristics.lck.total / 10);
    var sanBonus = Math.floor(data.characteristics.san.total / 10);
    var sagBonus = Math.floor(data.characteristics.sag.total / 10);

  //Set Campaign Rank
  if (data.xpTotal >= 5000) {
    data.campaignRank = "Maitre"
  } else if (data.xpTotal >= 4000) {
    data.campaignRank = "Expert"
  } else if (data.xpTotal >= 3000) {
    data.campaignRank = "Adepte"
  } else if (data.xpTotal >= 2000) {
    data.campaignRank = "Compagnon"
  } else {
    data.campaignRank = "Apprentie"
  }

    //Talent/Power/Trait Resource Bonuses
    data.hp.bonus = this._hpBonus(actorData);
    data.magicka.bonus = this._mpBonus(actorData);
    data.stamina.bonus = this._spBonus(actorData);
    data.luck_points.bonus = this._lpBonus(actorData);
    data.wound_threshold.bonus = this._wtBonus(actorData);
    data.speed.bonus = this._speedBonus(actorData);
    data.initiative.bonus = this._iniBonus(actorData);

    //Talent/Power/Trait Resistance Bonuses
    data.resistance.physicRBonus = this._physicRBonus(actorData);
    data.resistance.magicRBonus = this._magicRBonus(actorData);
    data.resistance.fireRBonus = this._fireRBonus(actorData);
    data.resistance.frostRBonus = this._frostRBonus(actorData);
    data.resistance.waterRBonus = this._waterRBonus(actorData);
    data.resistance.airRBonus = this._airRBonus(actorData);
    data.resistance.poisonRBonus = this._poisonRBonus(actorData);
    data.resistance.earthRBonus = this._earthRBonus(actorData);
    data.resistance.natureRBonus = this._natureRBonus(actorData);
    data.resistance.windRBonus = this._windRBonus(actorData);
    data.resistance.electricityRBonus = this._electricityRBonus(actorData);
    data.resistance.darkRBonus = this._darkRBonus(actorData);
    data.resistance.lightRBonus = this._lightRBonus(actorData);

    //Derived Calculations
    if (this._isMechanical(actorData) == true) {
      data.wound_threshold.base = strBonus + (endBonus * 2);
    } else {
      data.wound_threshold.base = strBonus + endBonus + wpBonus + (data.wound_threshold.bonus);
    }
    data.wound_threshold.value = data.wound_threshold.base;
    data.wound_threshold.value = this._woundThresholdCalc(actorData);
    
    data.speed.base = strBonus + (2 * agiBonus) + (data.speed.bonus);
    data.speed.value = this._speedCalc(actorData);
    data.speed.swimSpeed = parseFloat(this._swimCalc(actorData)) + parseFloat((data.speed.value/2).toFixed(0));
    data.speed.flySpeed = this._flyCalc(actorData);

    data.initiative.base = agiBonus + intBonus + prcBonus + (data.initiative.bonus);
    data.initiative.value = data.initiative.base;
    data.initiative.value = this._iniCalc(actorData);
    
    data.hp.max;
    data.magicka.max = data.magicka.base + this._mpBonus(actorData);
    data.stamina.max = endBonus + data.stamina.bonus;
    data.luck_points.max = lckBonus + data.luck_points.bonus;

    data.resistance.physicR.max = data.resistance.physicR.base + this._physicRBonus(actorData);
    data.resistance.magicR.max = data.resistance.magicR.base + this._magicRBonus(actorData);
    data.resistance.fireR.max = data.resistance.fireR.base + this._fireRBonus(actorData);
    data.resistance.frostR.max = data.resistance.frostR.base + this._frostRBonus(actorData);
    data.resistance.waterR.max = data.resistance.waterR.base + this._waterRBonus(actorData);
    data.resistance.airR.max = data.resistance.airR.base + this._airRBonus(actorData);
    data.resistance.poisonR.max = data.resistance.poisonR.base + this._poisonRBonus(actorData);
    data.resistance.earthR.max = data.resistance.earthR.base + this._earthRBonus(actorData);
    data.resistance.natureR.max = data.resistance.natureR.base + this._natureRBonus(actorData);
    data.resistance.windR.max = data.resistance.windR.base + this._windRBonus(actorData);
    data.resistance.electricityR.max = data.resistance.electricityR.base + this._electricityRBonus(actorData);
    data.resistance.darkR.max = data.resistance.darkR.base + this._darkRBonus(actorData);
    data.resistance.lightR.max = data.resistance.lightR.base + this._lightRBonus(actorData);

    data.carry_rating.max = Math.floor((4 * strBonus) + (2 * endBonus)) + data.carry_rating.bonus;
    data.carry_rating.current = this._armorWeight(actorData).toFixed(1);

    //Form Shift Calcs
    if (this._wereWolfForm(actorData) === true) {
      data.resistance.silverR = data.resistance.silverR - 5;
      data.resistance.diseaseR = data.resistance.diseaseR + 200;
      data.hp.max = data.hp.max + 5;
      data.stamina.max = data.stamina.max + 1;
      data.speed.base = data.speed.base + 9;
      data.speed.value = this._speedCalc(actorData);
      data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
      data.resistance.natToughness = 5;
      data.wound_threshold.value = data.wound_threshold.value + 5;
      data.action_points.max = data.action_points.max - 1;
      actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBatForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
      actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBoarForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.speed.base = data.speed.base + 9;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 7;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereBearForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 10;
        data.stamina.max = data.stamina.max + 1;
        data.speed.base = data.speed.base + 5;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereCrocodileForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._addHalfSpeed(actorData)).toFixed(0);
        data.speed.swimSpeed = parseFloat(this._speedCalc(actorData)) + 9;
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._wereVultureForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
        actorData.items.find(i => i.name === 'Survival').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Navigate').data.data.miscValue = 30;
        actorData.items.find(i => i.name === 'Observe').data.data.miscValue = 30;
    } else if (this._vampireLordForm(actorData) === true) {
        data.resistance.fireR = data.resistance.fireR - 1;
        data.resistance.sunlightR = data.resistance.sunlightR - 1;
        data.speed.flySpeed = 5;
        data.hp.max = data.hp.max + 5;
        data.magicka.max = data.magicka.max + 25;
        data.resistance.natToughness = 3;
    }

    //Speed Recalculation
    data.speed.value = this._addHalfSpeed(actorData);

    //ENC Burden Calculations
    if (data.carry_rating.current > data.carry_rating.max * 3) {
      data.carry_rating.label = 'Crushing'
      data.carry_rating.penalty = -40
      data.speed.value = 0;
      data.stamina.max = data.stamina.max - 5;
    } else if (data.carry_rating.current > data.carry_rating.max * 2) {
      data.carry_rating.label = 'Severe'
      data.carry_rating.penalty = -20
      data.speed.value = Math.floor(data.speed.base / 2);
      data.stamina.max = data.stamina.max - 3;
    } else if (data.carry_rating.current > data.carry_rating.max) {
      data.carry_rating.label = 'Moderate'
      data.carry_rating.penalty = 0
      data.speed.value = data.speed.value - 1;
      data.stamina.max = data.stamina.max - 1;
    } else if (data.carry_rating.current <= data.carry_rating.max) {
      data.carry_rating.label = "Minimal"
      data.carry_rating.penalty = 0
    }

    //Armor Weight Class Calculations
    if (data.armor_class == "super_heavy") {
      data.speed.value = data.speed.value - 3;
      data.speed.swimSpeed = data.speed.swimSpeed - 3;
    } else if (data.armor_class == "heavy") {
      data.speed.value = data.speed.value - 2;
      data.speed.swimSpeed = data.speed.swimSpeed - 2;
    } else if (data.armor_class == "medium") {
      data.speed.value = data.speed.value - 1;
      data.speed.swimSpeed = data.speed.swimSpeed - 1;
    } else {
      data.speed.value = data.speed.value;
      data.speed.swimSpeed = data.speed.swimSpeed;
    }

    //Wounded Penalties
    if (data.wounded == true) {
      let woundPen = 0
      let woundIni = -2;
      this._painIntolerant(actorData) ? woundPen = -30 : woundPen = -20

      if (this._halfWoundPenalty(actorData) === true) {
        data.woundPenalty = woundPen / 2
        data.initiative.value = data.initiative.base + (woundIni / 2);

      } else if (this._halfWoundPenalty(actorData) === false) {
        data.initiative.value = data.initiative.base + woundIni;
        data.woundPenalty = woundPen;
      }
    }

    //Fatigue Penalties
    data.fatigue.level = data.stamina.value <= 0 ? ((data.stamina.value -1) * -1) + data.fatigue.bonus : 0 + data.fatigue.bonus

    switch (data.fatigue.level > 0) {
      case true:
        data.fatigue.penalty = this._calcFatiguePenalty(actorData)
        break

      case false:
        data.fatigue.level = 0
        data.fatigue.penalty = 0
        break
    }

  } 

  async _prepareNPCData(actorData) {
    const data = actorData.data;

    //Add bonuses from items to Characteristics
    data.characteristics.str.total = data.characteristics.str.base + this._strBonusCalc(actorData);
    data.characteristics.end.total = data.characteristics.end.base + this._endBonusCalc(actorData);
    data.characteristics.agi.total = data.characteristics.agi.base + this._agiBonusCalc(actorData) - this._armorWeight(actorData);
    data.characteristics.int.total = data.characteristics.int.base + this._intBonusCalc(actorData);
    data.characteristics.wp.total = data.characteristics.wp.base + this._wpBonusCalc(actorData);
    data.characteristics.prc.total = data.characteristics.prc.base + this._prcBonusCalc(actorData);
    data.characteristics.prs.total = data.characteristics.prs.base + this._prsBonusCalc(actorData);
    data.characteristics.lck.total = data.characteristics.lck.base + this._lckBonusCalc(actorData);
    data.characteristics.san.total = data.characteristics.san.base + this._sanBonusCalc(actorData);
    data.characteristics.sag.total = data.characteristics.sag.base + this._sagBonusCalc(actorData);


    //Characteristic Bonuses
    var strBonus = Math.floor(data.characteristics.str.total / 10);
    var endBonus = Math.floor(data.characteristics.end.total / 10);
    var agiBonus = Math.floor(data.characteristics.agi.total / 10);
    var intBonus = Math.floor(data.characteristics.int.total / 10);
    var wpBonus = Math.floor(data.characteristics.wp.total / 10);
    var prcBonus = Math.floor(data.characteristics.prc.total / 10);
    var prsBonus = Math.floor(data.characteristics.prs.total / 10);
    var lckBonus = Math.floor(data.characteristics.lck.total / 10);
    var sanBonus = Math.floor(data.characteristics.san.total / 10);
    var sagBonus = Math.floor(data.characteristics.sag.total / 10);

    //Talent/Power/Trait Bonuses
    data.hp.bonus = this._hpBonus(actorData);
    data.magicka.bonus = this._mpBonus(actorData);
    data.stamina.bonus = this._spBonus(actorData);
    data.luck_points.bonus = this._lpBonus(actorData);
    data.wound_threshold.bonus = this._wtBonus(actorData);
    data.speed.bonus = this._speedBonus(actorData);
    data.initiative.bonus = this._iniBonus(actorData);

    //Talent/Power/Trait Resistance Bonuses
    data.resistance.physicRBonus = this._physicRBonus(actorData);
    data.resistance.magicRBonus = this._magicRBonus(actorData);
    data.resistance.fireRBonus = this._fireRBonus(actorData);
    data.resistance.frostRBonus = this._frostRBonus(actorData);
    data.resistance.waterRBonus = this._waterRBonus(actorData);
    data.resistance.airRBonus = this._airRBonus(actorData);
    data.resistance.poisonRBonus = this._poisonRBonus(actorData);
    data.resistance.earthRBonus = this._earthRBonus(actorData);
    data.resistance.natureRBonus = this._natureRBonus(actorData);
    data.resistance.windRBonus = this._windRBonus(actorData);
    data.resistance.electricityRBonus = this._electricityRBonus(actorData);
    data.resistance.darkRBonus = this._darkRBonus(actorData);
    data.resistance.lightRBonus = this._lightRBonus(actorData);

    //Derived Calculations
    if (this._isMechanical(actorData) == true) {
      data.wound_threshold.base = strBonus + (endBonus * 2);
    } else {
      data.wound_threshold.base = strBonus + endBonus + wpBonus + (data.wound_threshold.bonus);
    }
    data.wound_threshold.value = data.wound_threshold.base;
    data.wound_threshold.value = this._woundThresholdCalc(actorData);

    if (this._dwemerSphere(actorData) == true) {
      data.speed.base = 16;
      data.professions.evade = 70;
    } else {
        data.speed.base = strBonus + (2 * agiBonus) + (data.speed.bonus);
    }
    data.speed.value = this._speedCalc(actorData);
    data.speed.swimSpeed = parseFloat(this._swimCalc(actorData)) + parseFloat((data.speed.value/2).toFixed(0));
    data.speed.flySpeed = this._flyCalc(actorData);

    data.initiative.base = agiBonus + intBonus + prcBonus + (data.initiative.bonus);
    data.initiative.value = data.initiative.base;
    data.initiative.value = this._iniCalc(actorData);


    data.hp.max = data.hp.base + this._hpBonus(actorData);
    data.magicka.max = data.magicka.base + this._mpBonus(actorData);
    data.stamina.max = endBonus + data.stamina.bonus;
    data.luck_points.max = lckBonus + data.luck_points.bonus;

    data.resistance.physicR.max = data.resistance.physicR.base + this._physicRBonus(actorData);
    data.resistance.magicR.max = data.resistance.magicR.base + this._magicRBonus(actorData);
    data.resistance.fireR.max = data.resistance.fireR.base + this._fireRBonus(actorData);
    data.resistance.frostR.max = data.resistance.frostR.base + this._frostRBonus(actorData);
    data.resistance.waterR.max = data.resistance.waterR.base + this._waterRBonus(actorData);
    data.resistance.airR.max = data.resistance.airR.base + this._airRBonus(actorData);
    data.resistance.poisonR.max = data.resistance.poisonR.base + this._poisonRBonus(actorData);
    data.resistance.earthR.max = data.resistance.earthR.base + this._earthRBonus(actorData);
    data.resistance.natureR.max = data.resistance.natureR.base + this._natureRBonus(actorData);
    data.resistance.windR.max = data.resistance.windR.base + this._windRBonus(actorData);
    data.resistance.electricityR.max = data.resistance.electricityR.base + this._electricityRBonus(actorData);
    data.resistance.darkR.max = data.resistance.darkR.base + this._darkRBonus(actorData);
    data.resistance.lightR.max = data.resistance.lightR.base + this._lightRBonus(actorData);

    data.effect_cat.burn.max = data.effect_cat.burn.base + this._burnBonus(actorData);
    data.effect_cat.wet.max = data.effect_cat.wet.base + this._wetBonus(actorData);
    data.effect_cat.freeze.max = data.effect_cat.freeze.base + this._freezeBonus(actorData);
    data.effect_cat.paralysis.max = data.effect_cat.paralysis.base + this._paralysisBonus(actorData);
    data.effect_cat.slow.max = data.effect_cat.slow.base + this._slowBonus(actorData);
    data.effect_cat.flash.max = data.effect_cat.flash.base + this._flashBonus(actorData);
    data.effect_cat.dark.max = data.effect_cat.dark.base + this._darkBonus(actorData);
    data.effect_cat.poison.max = data.effect_cat.poison.base + this._poisonBonus(actorData);

    data.carry_rating.max = Math.floor((4 * strBonus) + (2 * endBonus)) + data.carry_rating.bonus;
    data.carry_rating.current = (this._calculateENC(actorData) - this._armorWeight(actorData) - this._excludeENC(actorData)).toFixed(1)

    //Form Shift Calcs
    if (this._wereWolfForm(actorData) === true) {
      data.resistance.silverR = data.resistance.silverR - 5;
      data.resistance.diseaseR = data.resistance.diseaseR + 200;
      data.hp.max = data.hp.max + 5;
      data.stamina.max = data.stamina.max + 1;
      data.speed.base = data.speed.base + 9;
      data.speed.value = this._speedCalc(actorData);
      data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
      data.resistance.natToughness = 5;
      data.wound_threshold.value = data.wound_threshold.value + 5;
      data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBatForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBoarForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.speed.base = data.speed.base + 9;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 7;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereBearForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 10;
        data.stamina.max = data.stamina.max + 1;
        data.speed.base = data.speed.base + 5;
        data.speed.value = this._speedCalc(actorData);
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;
    } else if (this._wereCrocodileForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._addHalfSpeed(actorData)).toFixed(0);
        data.speed.swimSpeed = parseFloat(this._speedCalc(actorData)) + 9;
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 5;
        data.action_points.max = data.action_points.max - 1;

    } else if (this._wereVultureForm(actorData) === true) {
        data.resistance.silverR = data.resistance.silverR - 5;
        data.resistance.diseaseR = data.resistance.diseaseR + 200;
        data.hp.max = data.hp.max + 5;
        data.stamina.max = data.stamina.max + 1;
        data.speed.value = (this._speedCalc(actorData)/2).toFixed(0);
        data.speed.flySpeed = data.speed.base + 9;
        data.speed.swimSpeed = (data.speed.value/2).toFixed(0);
        data.resistance.natToughness = 5;
        data.wound_threshold.value = data.wound_threshold.value + 3;
        data.action_points.max = data.action_points.max - 1;
    }else if (this._vampireLordForm(actorData) === true) {
        data.resistance.fireR = data.resistance.fireR - 1;
        data.resistance.sunlightR = data.resistance.sunlightR - 1;
        data.speed.flySpeed = 5;
        data.hp.max = data.hp.max + 5;
        data.magicka.max = data.magicka.max + 25;
        data.resistance.natToughness = 3;
    }

    //Speed Recalculation
    data.speed.value = this._addHalfSpeed(actorData);

    //ENC Burden Calculations
    if (game.settings.get('deadend-d100-FR', 'npcENCPenalty')) {
      if (data.carry_rating.current > data.carry_rating.max * 3) {
        data.carry_rating.label = 'Crushing'
        data.carry_rating.penalty = -40
        data.speed.value = 0;
        data.stamina.max = data.stamina.max - 5;
      } else if (data.carry_rating.current > data.carry_rating.max * 2) {
        data.carry_rating.label = 'Severe'
        data.carry_rating.penalty = -20
        data.speed.value = Math.floor(data.speed.base / 2);
        data.stamina.max = data.stamina.max - 3;
      } else if (data.carry_rating.current > data.carry_rating.max) {
        data.carry_rating.label = 'Moderate'
        data.carry_rating.penalty = 0
        data.speed.value = data.speed.value - 1;
        data.stamina.max = data.stamina.max - 1;
      } else if (data.carry_rating.current <= data.carry_rating.max) {
        data.carry_rating.label = "Minimal"
        data.carry_rating.penalty = 0
      }
    }

    //Armor Weight Class Calculations
    if (data.armor_class == "super_heavy") {
      data.speed.value = data.speed.value - 3;
      data.speed.swimSpeed = data.speed.swimSpeed - 3;
    } else if (data.armor_class == "heavy") {
      data.speed.value = data.speed.value - 2;
      data.speed.swimSpeed = data.speed.swimSpeed - 2;
    } else if (data.armor_class == "medium") {
      data.speed.value = data.speed.value - 1;
      data.speed.swimSpeed = data.speed.swimSpeed - 1;
    } else {
      data.speed.value = data.speed.value;
      data.speed.swimSpeed = data.speed.swimSpeed;
    }


    // Set Skill professions to regular professions (This is a fucking mess, but it's the way it's done for now...)
    for (let prof in data.professions) {
      if (prof === 'profession1'||prof === 'profession2'||prof === 'profession3'||prof === 'commerce') {
        data.professions[prof] === 0 ? data.professions[prof] = data.skills[prof].tn : data.professions[prof] = 0
      }
    }


    // Wound Penalties
    if (data.wounded === true) {
      let woundPen = 0
      let woundIni = -2;
      this._painIntolerant(actorData) ? woundPen = -30 : woundPen = -20

      if (this._halfWoundPenalty(actorData) === true) {
        for (var skill in data.professionsWound) {
          data.professionsWound[skill] = data.professions[skill] + (woundPen / 2);
        }

        data.woundPenalty = woundPen / 2
        data.initiative.value = data.initiative.base + (woundIni / 2);

      } 

      else if (this._halfWoundPenalty(actorData) === false) {
        for (var skill in data.professionsWound) {
          data.professionsWound[skill] = data.professions[skill] + woundPen;
        }

        data.initiative.value = data.initiative.base + woundIni;
        data.woundPenalty = woundPen;

        }
      } 
      
      else if (data.wounded === false) {
          for (var skill in data.professionsWound) {
           data.professionsWound[skill] = data.professions[skill];
        }
      }

    //Fatigue Penalties
    data.fatigue.level = data.stamina.value <= 0 ? ((data.stamina.value -1) * -1) + data.fatigue.bonus : 0 + data.fatigue.bonus

    switch (data.fatigue.level > 0) {
      case true:
        data.fatigue.penalty = this._calcFatiguePenalty(actorData)
        break

      case false:
        data.fatigue.level = 0
        data.fatigue.penalty = 0
        break
    }

    // Set Lucky/Unlucky Numbers based on Threat Category
    if (data.threat == "minorSolo") {
      data.unlucky_numbers.ul1 = 95;
      data.unlucky_numbers.ul2 = 96;
      data.unlucky_numbers.ul3 = 97;
      data.unlucky_numbers.ul4 = 98;
      data.unlucky_numbers.ul5 = 99;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 0;
      data.lucky_numbers.ln2 = 0;
      data.lucky_numbers.ln3 = 0;
      data.lucky_numbers.ln4 = 0;
      data.lucky_numbers.ln5 = 0;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "minorGroup") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 96;
      data.unlucky_numbers.ul3 = 97;
      data.unlucky_numbers.ul4 = 98;
      data.unlucky_numbers.ul5 = 99;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 0;
      data.lucky_numbers.ln3 = 0;
      data.lucky_numbers.ln4 = 0;
      data.lucky_numbers.ln5 = 0;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "majorSolo") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 97;
      data.unlucky_numbers.ul4 = 98;
      data.unlucky_numbers.ul5 = 99;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 0;
      data.lucky_numbers.ln4 = 0;
      data.lucky_numbers.ln5 = 0;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "majorGroup") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 0;
      data.unlucky_numbers.ul4 = 98;
      data.unlucky_numbers.ul5 = 99;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 3;
      data.lucky_numbers.ln4 = 0;
      data.lucky_numbers.ln5 = 0;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "deadlySolo") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 0;
      data.unlucky_numbers.ul4 = 0;
      data.unlucky_numbers.ul5 = 99;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 3;
      data.lucky_numbers.ln4 = 4;
      data.lucky_numbers.ln5 = 0;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "deadlyGroup") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 0;
      data.unlucky_numbers.ul4 = 0;
      data.unlucky_numbers.ul5 = 0;
      data.unlucky_numbers.ul6 = 100;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 3;
      data.lucky_numbers.ln4 = 4;
      data.lucky_numbers.ln5 = 5;
      data.lucky_numbers.ln6 = 0;
      data.lucky_numbers.ln7 = 0;
      data.lucky_numbers.ln8 = 0;
      data.lucky_numbers.ln9 = 0;
      data.lucky_numbers.ln10 = 0;
    } else if (data.threat == "legendarySolo") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 0;
      data.unlucky_numbers.ul4 = 0;
      data.unlucky_numbers.ul5 = 0;
      data.unlucky_numbers.ul6 = 0;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 3;
      data.lucky_numbers.ln4 = 4;
      data.lucky_numbers.ln5 = 5;
      data.lucky_numbers.ln6 = 6;
      data.lucky_numbers.ln7 = 7;
      data.lucky_numbers.ln8 = 8;
      data.lucky_numbers.ln9 = 9;
      data.lucky_numbers.ln10 = 10;
    } else if (data.threat == "legendaryGroup") {
      data.unlucky_numbers.ul1 = 0;
      data.unlucky_numbers.ul2 = 0;
      data.unlucky_numbers.ul3 = 0;
      data.unlucky_numbers.ul4 = 0;
      data.unlucky_numbers.ul5 = 0;
      data.unlucky_numbers.ul6 = 0;
      data.lucky_numbers.ln1 = 1;
      data.lucky_numbers.ln2 = 2;
      data.lucky_numbers.ln3 = 3;
      data.lucky_numbers.ln4 = 4;
      data.lucky_numbers.ln5 = 5;
      data.lucky_numbers.ln6 = 6;
      data.lucky_numbers.ln7 = 7;
      data.lucky_numbers.ln8 = 8;
      data.lucky_numbers.ln9 = 9;
      data.lucky_numbers.ln10 = 10;
    }

    // Calculate Item Profession Modifiers
    this._calculateItemSkillModifiers(actorData)

  }

  async _calculateItemSkillModifiers(actorData) {
    let modItems = actorData.items.filter(i => 
      i.data.data.hasOwnProperty('skillArray')
      && i.data.data.skillArray.length > 0
      && i.data.data.equipped
    )

    for (let item of modItems) {
      for (let entry of item.data.data.skillArray) {
        let moddedSkill = actorData.data.professions[entry.name]
        actorData.data.professions[entry.name] = Number(moddedSkill) + Number(entry.value)
        actorData.data.professionsWound[entry.name] = Number(moddedSkill) + Number(entry.value)
      }
    }
  }

  _strBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.strChaBonus;
    }
    return totalBonus
  }

  _endBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.endChaBonus;
    }
    return totalBonus
  }

  _agiBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.agiChaBonus;
    }
    return totalBonus
  }

  _intBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.intChaBonus;
    }
    return totalBonus
  }

  _wpBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.wpChaBonus;
    }
    return totalBonus
  }

  _prcBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.prcChaBonus;
    }
    return totalBonus
  }

  _prsBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.prsChaBonus;
    }
    return totalBonus
  }

  _lckBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.lckChaBonus;
    }
    return totalBonus
  }

  _sanBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.sanChaBonus;
    }
    return totalBonus
  }

  _sagBonusCalc(actorData) {
    let strBonusItems = actorData.items.filter(item => item.data.data.hasOwnProperty("characteristicBonus"));
    let totalBonus = 0;
    for (let item of strBonusItems) {
      totalBonus = totalBonus + item.data.data.characteristicBonus.sagChaBonus;
    }
    return totalBonus
  }

  _calculateENC(actorData) {
    let weighted = actorData.items.filter(item => item.data.data.hasOwnProperty("enc"));
    let totalWeight = 0.0;
    for (let item of weighted) {
      totalWeight = totalWeight + (item.data.data.enc * item.data.data.quantity);
    }
    return totalWeight
  }

  _armorWeight(actorData) {
    let worn = actorData.items.filter(item => item.data.data.equipped == true);
    let armorENC = 0.0;
    for (let item of worn) {
      armorENC = armorENC + ((item.data.data.enc) * item.data.data.quantity);
    } 
    return armorENC
  }

  _excludeENC(actorData) {
    let excluded = actorData.items.filter(item => item.data.data.excludeENC == true);
    let totalWeight = 0.0;
    for (let item of excluded) {
      totalWeight = totalWeight + (item.data.data.enc * item.data.data.quantity);
    }
    return totalWeight
  }

  _hpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("hpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.hpBonus;
    }
    return bonus
  }

  _mpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("mpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.mpBonus;
    }
    return bonus
  }

  _spBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("spBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.spBonus;
    }
    return bonus
  }

  _lpBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("lpBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.lpBonus;
    }
    return bonus
  }

  _wtBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("wtBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.wtBonus;
    }
    return bonus
  }

  _speedBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("speedBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.speedBonus;
    }
    return bonus
  }

  _iniBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("iniBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.iniBonus;
    }
    return bonus
  }

  _diseaseR(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("diseaseR"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.diseaseR;
    }
    return bonus
  }

  //Resistances
  _fireRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("fireRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.fireRBonus;
      }
      return bonus
  }

  _frostRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("frostRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.frostRBonus;
      }
      return bonus
  }

  _poisonRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("poisonRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.poisonRBonus;
      }
      return bonus
  }

  _magicRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("magicRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.magicRBonus;
      }
      return bonus
  }

  _physicRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("physicRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.physicRBonus;
      }
      return bonus
  }

  _waterRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("waterRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.waterRBonus;
      }
      return bonus
  }

  _airRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("airRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.airRBonus;
      }
      return bonus
  }

  _earthRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("earthRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.earthRBonus;
      }
      return bonus
  }

  _natureRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("natureRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.natureRBonus;
      }
      return bonus
  }

  _windRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("windRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.windRBonus;
      }
      return bonus
  }

  _electricityRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("electricityRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.electricityRBonus;
      }
      return bonus
  }

  _darkRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("darkRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.darkRBonus;
      }
      return bonus
  }

  _lightRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("lightRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.lightRBonus;
      }
      return bonus
  }

  _burnBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("burnBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.burnBonus;
      }
      return bonus
  }

  _wetBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("wetBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.wetBonus;
      }
      return bonus
  }

  _freezeBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("freezeBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.freezeBonus;
      }
      return bonus
  }

  _paralysisBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("paralysisBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.paralysisBonus;
      }
      return bonus
  }

  _slowBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("slowBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.slowBonus;
      }
      return bonus
  }

  _flashBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("flashBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.flashBonus;
      }
      return bonus
  }

  _darkBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("darkBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.darkBonus;
      }
      return bonus
  }

  _poisonBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("poisonBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.poisonBonus;
      }
      return bonus
  }

  _shockRBonus(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("shockRBonus"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.shockRBonus;
      }
      return bonus
  }

  _natToughnessR(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("natToughnessR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.natToughnessR;
      }
      return bonus
  }

  _silverR(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("silverR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.silverR;
      }
      return bonus
  }

  _sunlightR(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("sunlightR"));
    let bonus = 0;
    for (let item of attribute) {
        bonus = bonus + item.data.data.sunlightR;
      }
      return bonus
  }

  _swimCalc(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("swimBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.swimBonus;
    }
    return bonus
  }

  _flyCalc(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.hasOwnProperty("flyBonus"));
    let bonus = 0;
    for (let item of attribute) {
      bonus = bonus + item.data.data.flyBonus;
    }
    return bonus
  }

  _speedCalc(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.halfSpeed === true);
    let speed = actorData.data.speed.base;
    if (attribute.length === 0) {
      speed = speed;
    } else if (attribute.length >= 1) {
      speed = Math.ceil(speed/2);
    }
    return speed;
  }

  _iniCalc(actorData) {
    let attribute = actorData.items.filter(item => item.type == "trait"|| item.type == "talent");
    let init = actorData.data.initiative.base;
      for (let item of attribute) {
        if (item.data.data.replace.ini.characteristic != "none") {
          if (item.data.data.replace.ini.characteristic == "str") {
            init = Math.floor(actorData.data.characteristics.str.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "end") {
            init = Math.floor(actorData.data.characteristics.end.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "agi") {
            init = Math.floor(actorData.data.characteristics.agi.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "int") {
            init = Math.floor(actorData.data.characteristics.int.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "wp") {
            init = Math.floor(actorData.data.characteristics.wp.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "prc") {
            init = Math.floor(actorData.data.characteristics.prc.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "prs") {
            init = Math.floor(actorData.data.characteristics.prs.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "lck") {
            init = Math.floor(actorData.data.characteristics.lck.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "san") {
            init = Math.floor(actorData.data.characteristics.san.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "sag") {
            init = Math.floor(actorData.data.characteristics.sag.total / 10) * 3;
          }
        }
      }
    return init;
  }

  _woundThresholdCalc(actorData) {
    let attribute = actorData.items.filter(item => item.type === "trait"|| item.type === "talent");
    let wound = actorData.data.wound_threshold.base;
      for (let item of attribute) {
        if (item.data.data.replace.wt.characteristic != "none") {
          if (item.data.data.replace.wt.characteristic === "str") {
            wound = Math.floor(actorData.data.characteristics.str.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "end") {
            wound = Math.floor(actorData.data.characteristics.end.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "agi") {
            wound = Math.floor(actorData.data.characteristics.agi.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "int") {
            wound = Math.floor(actorData.data.characteristics.int.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "wp") {
            wound = Math.floor(actorData.data.characteristics.wp.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "prc") {
            wound = Math.floor(actorData.data.characteristics.prc.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "prs") {
            wound = Math.floor(actorData.data.characteristics.prs.total / 10) * 3;
          } else if (item.data.data.replace.wt.characteristic === "lck") {
            wound = Math.floor(actorData.data.characteristics.lck.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "san") {
            init = Math.floor(actorData.data.characteristics.san.total / 10) * 3;
          } else if (item.data.data.replace.ini.characteristic == "sag") {
            init = Math.floor(actorData.data.characteristics.sag.total / 10) * 3;
          }
        }
      }
    return wound;
  }

  _calcFatiguePenalty(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.halfFatiguePenalty == true);
    let penalty = 0;

    return penalty
  }

  _halfWoundPenalty(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.halfWoundPenalty == true);
    let woundReduction = false;
    if (attribute.length >= 1) {
      woundReduction = true;
    } else {
      woundReduction = false;
    }
    return woundReduction
  }

  _addIntToMP(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.addIntToMP == true);
    let mp = 0;
    if (attribute.length >= 1) {
      mp = actorData.data.characteristics.int.total;
    } else {
      mp = 0;
    }
    return mp
  }

  _untrainedException(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.untrainedException == true);
    const legacyUntrained = game.settings.get("deadend-d100-FR", "legacyUntrainedPenalty");
    let x = 0;
    if (legacyUntrained) {
      if (attribute.length >= 1) {
        x = 20;
      }
    } else if (attribute.length >= 1) {
      x = 10;
    }
    return x
  }

  _isMechanical(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.mechanical == true);
    let isMechanical = false;
    if (attribute.length >= 1) {
      isMechanical = true;
    } else {
      isMechanical = false;
    }
    return isMechanical
  }

  _dwemerSphere(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.shiftForm == true);
    let shift = false;
    if (attribute.length >= 1) {
      for (let item of attribute) {
        if (item.data.data.dailyUse == true) {
          shift = true;
        }
      }
    } else {
      shift = false;
    }
    return shift
  }

  _vampireLordForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormVampireLord");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereWolfForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereWolf"||item.data.data.shiftFormStyle === "shiftFormWereLion");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBatForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereBat");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBoarForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereBoar");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereBearForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereBear");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereCrocodileForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereCrocodile");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _wereVultureForm(actorData) {
    let form = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereVulture");
    let shift = false;
    if(form.length > 0) {
      shift = true;
    }
    return shift
  }

  _painIntolerant(actorData) {
    let attribute = actorData.items.filter(item => item.data.data.painIntolerant == true);
    let pain = false;
    if (attribute.length >= 1) {
      pain = true;
    } 
    return pain
  }

  _addHalfSpeed(actorData) {
    let halfSpeedItems = actorData.items.filter(item => item.data.data.addHalfSpeed === true);
    let isWereCroc = actorData.items.filter(item => item.data.data.shiftFormStyle === "shiftFormWereCrocodile");
    let speed = actorData.data.speed.value;
    if (isWereCroc.length > 0 && halfSpeedItems.length > 0) {
      speed = actorData.data.speed.base;
    } else if (isWereCroc.length == 0 && halfSpeedItems.length > 0) {
      speed = Math.ceil(actorData.data.speed.value/2) + actorData.data.speed.base;
    } else if (isWereCroc.length > 0 && halfSpeedItems.length == 0) {
      speed = Math.ceil(actorData.data.speed.base/2);
    } else {
      speed = actorData.data.speed.value;
    }
    return speed
  }

}
